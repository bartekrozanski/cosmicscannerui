﻿namespace CosmicScannerUI
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.sensor1GroupBox = new System.Windows.Forms.GroupBox();
            this.numericUpDown3 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown2 = new System.Windows.Forms.NumericUpDown();
            this.sensor3GroupBox = new System.Windows.Forms.GroupBox();
            this.numericUpDown4 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown5 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown6 = new System.Windows.Forms.NumericUpDown();
            this.sensor2GroupBox = new System.Windows.Forms.GroupBox();
            this.numericUpDown7 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown8 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown9 = new System.Windows.Forms.NumericUpDown();
            this.sensor4GroupBox = new System.Windows.Forms.GroupBox();
            this.numericUpDown10 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown11 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown12 = new System.Windows.Forms.NumericUpDown();
            this.sensorsGroupBox = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.visibilityGroupBox = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.numericUpDown13 = new System.Windows.Forms.NumericUpDown();
            this.allDistRadioButton = new System.Windows.Forms.RadioButton();
            this.greaterDistRadioButton = new System.Windows.Forms.RadioButton();
            this.lessDistRadioButton = new System.Windows.Forms.RadioButton();
            this.label4 = new System.Windows.Forms.Label();
            this.swapBottom = new System.Windows.Forms.Button();
            this.swapTop = new System.Windows.Forms.Button();
            this.swapDataGroupBox = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.maxAngleNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.minAngleNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.allAngleRadioButton = new System.Windows.Forms.RadioButton();
            this.angleNotBetweenRadioButton = new System.Windows.Forms.RadioButton();
            this.angleBetweenRadioButton = new System.Windows.Forms.RadioButton();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.sensor1GroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).BeginInit();
            this.sensor3GroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown6)).BeginInit();
            this.sensor2GroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown9)).BeginInit();
            this.sensor4GroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown12)).BeginInit();
            this.sensorsGroupBox.SuspendLayout();
            this.visibilityGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown13)).BeginInit();
            this.swapDataGroupBox.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.maxAngleNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.minAngleNumericUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.DecimalPlaces = 6;
            this.numericUpDown1.Location = new System.Drawing.Point(6, 19);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numericUpDown1.Minimum = new decimal(new int[] {
            100000,
            0,
            0,
            -2147483648});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(120, 20);
            this.numericUpDown1.TabIndex = 0;
            this.numericUpDown1.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // sensor1GroupBox
            // 
            this.sensor1GroupBox.Controls.Add(this.numericUpDown3);
            this.sensor1GroupBox.Controls.Add(this.numericUpDown2);
            this.sensor1GroupBox.Controls.Add(this.numericUpDown1);
            this.sensor1GroupBox.Location = new System.Drawing.Point(34, 19);
            this.sensor1GroupBox.Name = "sensor1GroupBox";
            this.sensor1GroupBox.Size = new System.Drawing.Size(136, 100);
            this.sensor1GroupBox.TabIndex = 1;
            this.sensor1GroupBox.TabStop = false;
            this.sensor1GroupBox.Text = "Sensor 1";
            // 
            // numericUpDown3
            // 
            this.numericUpDown3.DecimalPlaces = 6;
            this.numericUpDown3.Location = new System.Drawing.Point(6, 71);
            this.numericUpDown3.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numericUpDown3.Minimum = new decimal(new int[] {
            100000,
            0,
            0,
            -2147483648});
            this.numericUpDown3.Name = "numericUpDown3";
            this.numericUpDown3.Size = new System.Drawing.Size(120, 20);
            this.numericUpDown3.TabIndex = 2;
            this.numericUpDown3.Value = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDown3.ValueChanged += new System.EventHandler(this.numericUpDown3_ValueChanged);
            // 
            // numericUpDown2
            // 
            this.numericUpDown2.DecimalPlaces = 6;
            this.numericUpDown2.Location = new System.Drawing.Point(6, 45);
            this.numericUpDown2.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numericUpDown2.Minimum = new decimal(new int[] {
            100000,
            0,
            0,
            -2147483648});
            this.numericUpDown2.Name = "numericUpDown2";
            this.numericUpDown2.Size = new System.Drawing.Size(120, 20);
            this.numericUpDown2.TabIndex = 1;
            this.numericUpDown2.ValueChanged += new System.EventHandler(this.numericUpDown2_ValueChanged);
            // 
            // sensor3GroupBox
            // 
            this.sensor3GroupBox.Controls.Add(this.numericUpDown4);
            this.sensor3GroupBox.Controls.Add(this.numericUpDown5);
            this.sensor3GroupBox.Controls.Add(this.numericUpDown6);
            this.sensor3GroupBox.Location = new System.Drawing.Point(318, 19);
            this.sensor3GroupBox.Name = "sensor3GroupBox";
            this.sensor3GroupBox.Size = new System.Drawing.Size(136, 100);
            this.sensor3GroupBox.TabIndex = 3;
            this.sensor3GroupBox.TabStop = false;
            this.sensor3GroupBox.Text = "Sensor 3";
            // 
            // numericUpDown4
            // 
            this.numericUpDown4.DecimalPlaces = 6;
            this.numericUpDown4.Location = new System.Drawing.Point(6, 71);
            this.numericUpDown4.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numericUpDown4.Minimum = new decimal(new int[] {
            100000,
            0,
            0,
            -2147483648});
            this.numericUpDown4.Name = "numericUpDown4";
            this.numericUpDown4.Size = new System.Drawing.Size(120, 20);
            this.numericUpDown4.TabIndex = 2;
            this.numericUpDown4.Value = new decimal(new int[] {
            9600,
            0,
            0,
            -2147483648});
            this.numericUpDown4.ValueChanged += new System.EventHandler(this.numericUpDown4_ValueChanged);
            // 
            // numericUpDown5
            // 
            this.numericUpDown5.DecimalPlaces = 6;
            this.numericUpDown5.Location = new System.Drawing.Point(6, 45);
            this.numericUpDown5.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numericUpDown5.Minimum = new decimal(new int[] {
            100000,
            0,
            0,
            -2147483648});
            this.numericUpDown5.Name = "numericUpDown5";
            this.numericUpDown5.Size = new System.Drawing.Size(120, 20);
            this.numericUpDown5.TabIndex = 1;
            this.numericUpDown5.ValueChanged += new System.EventHandler(this.numericUpDown5_ValueChanged);
            // 
            // numericUpDown6
            // 
            this.numericUpDown6.DecimalPlaces = 6;
            this.numericUpDown6.Location = new System.Drawing.Point(6, 19);
            this.numericUpDown6.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numericUpDown6.Minimum = new decimal(new int[] {
            100000,
            0,
            0,
            -2147483648});
            this.numericUpDown6.Name = "numericUpDown6";
            this.numericUpDown6.Size = new System.Drawing.Size(120, 20);
            this.numericUpDown6.TabIndex = 0;
            this.numericUpDown6.ValueChanged += new System.EventHandler(this.numericUpDown6_ValueChanged);
            // 
            // sensor2GroupBox
            // 
            this.sensor2GroupBox.Controls.Add(this.numericUpDown7);
            this.sensor2GroupBox.Controls.Add(this.numericUpDown8);
            this.sensor2GroupBox.Controls.Add(this.numericUpDown9);
            this.sensor2GroupBox.Location = new System.Drawing.Point(176, 19);
            this.sensor2GroupBox.Name = "sensor2GroupBox";
            this.sensor2GroupBox.Size = new System.Drawing.Size(136, 100);
            this.sensor2GroupBox.TabIndex = 3;
            this.sensor2GroupBox.TabStop = false;
            this.sensor2GroupBox.Text = "Sensor 2";
            // 
            // numericUpDown7
            // 
            this.numericUpDown7.DecimalPlaces = 6;
            this.numericUpDown7.Location = new System.Drawing.Point(6, 71);
            this.numericUpDown7.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numericUpDown7.Minimum = new decimal(new int[] {
            100000,
            0,
            0,
            -2147483648});
            this.numericUpDown7.Name = "numericUpDown7";
            this.numericUpDown7.Size = new System.Drawing.Size(120, 20);
            this.numericUpDown7.TabIndex = 2;
            this.numericUpDown7.Value = new decimal(new int[] {
            9600,
            0,
            0,
            0});
            this.numericUpDown7.ValueChanged += new System.EventHandler(this.numericUpDown7_ValueChanged);
            // 
            // numericUpDown8
            // 
            this.numericUpDown8.DecimalPlaces = 6;
            this.numericUpDown8.Location = new System.Drawing.Point(6, 45);
            this.numericUpDown8.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numericUpDown8.Minimum = new decimal(new int[] {
            100000,
            0,
            0,
            -2147483648});
            this.numericUpDown8.Name = "numericUpDown8";
            this.numericUpDown8.Size = new System.Drawing.Size(120, 20);
            this.numericUpDown8.TabIndex = 1;
            this.numericUpDown8.ValueChanged += new System.EventHandler(this.numericUpDown8_ValueChanged);
            // 
            // numericUpDown9
            // 
            this.numericUpDown9.DecimalPlaces = 6;
            this.numericUpDown9.Location = new System.Drawing.Point(6, 19);
            this.numericUpDown9.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numericUpDown9.Minimum = new decimal(new int[] {
            100000,
            0,
            0,
            -2147483648});
            this.numericUpDown9.Name = "numericUpDown9";
            this.numericUpDown9.Size = new System.Drawing.Size(120, 20);
            this.numericUpDown9.TabIndex = 0;
            this.numericUpDown9.ValueChanged += new System.EventHandler(this.numericUpDown9_ValueChanged);
            // 
            // sensor4GroupBox
            // 
            this.sensor4GroupBox.Controls.Add(this.numericUpDown10);
            this.sensor4GroupBox.Controls.Add(this.numericUpDown11);
            this.sensor4GroupBox.Controls.Add(this.numericUpDown12);
            this.sensor4GroupBox.Location = new System.Drawing.Point(460, 19);
            this.sensor4GroupBox.Name = "sensor4GroupBox";
            this.sensor4GroupBox.Size = new System.Drawing.Size(136, 100);
            this.sensor4GroupBox.TabIndex = 3;
            this.sensor4GroupBox.TabStop = false;
            this.sensor4GroupBox.Text = "Sensor 4";
            // 
            // numericUpDown10
            // 
            this.numericUpDown10.DecimalPlaces = 6;
            this.numericUpDown10.Location = new System.Drawing.Point(6, 71);
            this.numericUpDown10.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numericUpDown10.Minimum = new decimal(new int[] {
            100000,
            0,
            0,
            -2147483648});
            this.numericUpDown10.Name = "numericUpDown10";
            this.numericUpDown10.Size = new System.Drawing.Size(120, 20);
            this.numericUpDown10.TabIndex = 2;
            this.numericUpDown10.Value = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.numericUpDown10.ValueChanged += new System.EventHandler(this.numericUpDown10_ValueChanged);
            // 
            // numericUpDown11
            // 
            this.numericUpDown11.DecimalPlaces = 6;
            this.numericUpDown11.Location = new System.Drawing.Point(6, 45);
            this.numericUpDown11.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numericUpDown11.Minimum = new decimal(new int[] {
            100000,
            0,
            0,
            -2147483648});
            this.numericUpDown11.Name = "numericUpDown11";
            this.numericUpDown11.Size = new System.Drawing.Size(120, 20);
            this.numericUpDown11.TabIndex = 1;
            this.numericUpDown11.ValueChanged += new System.EventHandler(this.numericUpDown11_ValueChanged);
            // 
            // numericUpDown12
            // 
            this.numericUpDown12.DecimalPlaces = 6;
            this.numericUpDown12.Location = new System.Drawing.Point(6, 19);
            this.numericUpDown12.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numericUpDown12.Minimum = new decimal(new int[] {
            100000,
            0,
            0,
            -2147483648});
            this.numericUpDown12.Name = "numericUpDown12";
            this.numericUpDown12.Size = new System.Drawing.Size(120, 20);
            this.numericUpDown12.TabIndex = 0;
            this.numericUpDown12.ValueChanged += new System.EventHandler(this.numericUpDown12_ValueChanged);
            // 
            // sensorsGroupBox
            // 
            this.sensorsGroupBox.Controls.Add(this.label3);
            this.sensorsGroupBox.Controls.Add(this.label2);
            this.sensorsGroupBox.Controls.Add(this.label1);
            this.sensorsGroupBox.Controls.Add(this.sensor1GroupBox);
            this.sensorsGroupBox.Controls.Add(this.sensor4GroupBox);
            this.sensorsGroupBox.Controls.Add(this.sensor2GroupBox);
            this.sensorsGroupBox.Controls.Add(this.sensor3GroupBox);
            this.sensorsGroupBox.Location = new System.Drawing.Point(13, 13);
            this.sensorsGroupBox.Name = "sensorsGroupBox";
            this.sensorsGroupBox.Size = new System.Drawing.Size(607, 128);
            this.sensorsGroupBox.TabIndex = 4;
            this.sensorsGroupBox.TabStop = false;
            this.sensorsGroupBox.Text = "Sensors Positions";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(14, 90);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(14, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Z";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(14, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Y";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(14, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "X";
            // 
            // visibilityGroupBox
            // 
            this.visibilityGroupBox.Controls.Add(this.label5);
            this.visibilityGroupBox.Controls.Add(this.numericUpDown13);
            this.visibilityGroupBox.Controls.Add(this.allDistRadioButton);
            this.visibilityGroupBox.Controls.Add(this.greaterDistRadioButton);
            this.visibilityGroupBox.Controls.Add(this.lessDistRadioButton);
            this.visibilityGroupBox.Controls.Add(this.label4);
            this.visibilityGroupBox.Location = new System.Drawing.Point(13, 157);
            this.visibilityGroupBox.Name = "visibilityGroupBox";
            this.visibilityGroupBox.Size = new System.Drawing.Size(409, 95);
            this.visibilityGroupBox.TabIndex = 5;
            this.visibilityGroupBox.TabStop = false;
            this.visibilityGroupBox.Text = "Distance visibility options";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(279, 45);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(33, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "value";
            // 
            // numericUpDown13
            // 
            this.numericUpDown13.DecimalPlaces = 6;
            this.numericUpDown13.Location = new System.Drawing.Point(318, 43);
            this.numericUpDown13.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numericUpDown13.Name = "numericUpDown13";
            this.numericUpDown13.Size = new System.Drawing.Size(83, 20);
            this.numericUpDown13.TabIndex = 4;
            this.numericUpDown13.ValueChanged += new System.EventHandler(this.numericUpDown13_ValueChanged);
            // 
            // allDistRadioButton
            // 
            this.allDistRadioButton.AutoSize = true;
            this.allDistRadioButton.Location = new System.Drawing.Point(242, 67);
            this.allDistRadioButton.Name = "allDistRadioButton";
            this.allDistRadioButton.Size = new System.Drawing.Size(66, 17);
            this.allDistRadioButton.TabIndex = 3;
            this.allDistRadioButton.TabStop = true;
            this.allDistRadioButton.Text = "Show All";
            this.allDistRadioButton.UseVisualStyleBackColor = true;
            this.allDistRadioButton.CheckedChanged += new System.EventHandler(this.allDistRadioButton_CheckedChanged);
            // 
            // greaterDistRadioButton
            // 
            this.greaterDistRadioButton.AutoSize = true;
            this.greaterDistRadioButton.Location = new System.Drawing.Point(242, 43);
            this.greaterDistRadioButton.Name = "greaterDistRadioButton";
            this.greaterDistRadioButton.Size = new System.Drawing.Size(31, 17);
            this.greaterDistRadioButton.TabIndex = 2;
            this.greaterDistRadioButton.TabStop = true;
            this.greaterDistRadioButton.Text = ">";
            this.greaterDistRadioButton.UseVisualStyleBackColor = true;
            this.greaterDistRadioButton.CheckedChanged += new System.EventHandler(this.greaterDistRadioButton_CheckedChanged);
            // 
            // lessDistRadioButton
            // 
            this.lessDistRadioButton.AutoSize = true;
            this.lessDistRadioButton.Location = new System.Drawing.Point(242, 20);
            this.lessDistRadioButton.Name = "lessDistRadioButton";
            this.lessDistRadioButton.Size = new System.Drawing.Size(31, 17);
            this.lessDistRadioButton.TabIndex = 1;
            this.lessDistRadioButton.TabStop = true;
            this.lessDistRadioButton.Text = "<";
            this.lessDistRadioButton.UseVisualStyleBackColor = true;
            this.lessDistRadioButton.CheckedChanged += new System.EventHandler(this.lessDistRadioButton_CheckedChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(18, 43);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(218, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Show points where distance between lines is";
            // 
            // swapBottom
            // 
            this.swapBottom.Location = new System.Drawing.Point(4, 17);
            this.swapBottom.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.swapBottom.Name = "swapBottom";
            this.swapBottom.Size = new System.Drawing.Size(128, 19);
            this.swapBottom.TabIndex = 6;
            this.swapBottom.Text = "Sensor1 <-> Sensor2";
            this.swapBottom.UseVisualStyleBackColor = true;
            this.swapBottom.Click += new System.EventHandler(this.swapBottom_Click);
            // 
            // swapTop
            // 
            this.swapTop.Location = new System.Drawing.Point(4, 45);
            this.swapTop.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.swapTop.Name = "swapTop";
            this.swapTop.Size = new System.Drawing.Size(128, 19);
            this.swapTop.TabIndex = 7;
            this.swapTop.Text = "Sensor3 <-> Sensor4";
            this.swapTop.UseVisualStyleBackColor = true;
            this.swapTop.Click += new System.EventHandler(this.swapTop_Click);
            // 
            // swapDataGroupBox
            // 
            this.swapDataGroupBox.Controls.Add(this.swapBottom);
            this.swapDataGroupBox.Controls.Add(this.swapTop);
            this.swapDataGroupBox.Location = new System.Drawing.Point(427, 157);
            this.swapDataGroupBox.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.swapDataGroupBox.Name = "swapDataGroupBox";
            this.swapDataGroupBox.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.swapDataGroupBox.Size = new System.Drawing.Size(142, 72);
            this.swapDataGroupBox.TabIndex = 8;
            this.swapDataGroupBox.TabStop = false;
            this.swapDataGroupBox.Text = "Swap data";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(18, 34);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(193, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "Show points where intersection angle is";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.maxAngleNumericUpDown);
            this.groupBox1.Controls.Add(this.minAngleNumericUpDown);
            this.groupBox1.Controls.Add(this.allAngleRadioButton);
            this.groupBox1.Controls.Add(this.angleNotBetweenRadioButton);
            this.groupBox1.Controls.Add(this.angleBetweenRadioButton);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Location = new System.Drawing.Point(13, 258);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox1.Size = new System.Drawing.Size(617, 90);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Angle visibility options";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(523, 40);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(45, 13);
            this.label8.TabIndex = 13;
            this.label8.Text = "degrees";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(399, 39);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(25, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "and";
            // 
            // maxAngleNumericUpDown
            // 
            this.maxAngleNumericUpDown.Location = new System.Drawing.Point(428, 39);
            this.maxAngleNumericUpDown.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.maxAngleNumericUpDown.Maximum = new decimal(new int[] {
            180,
            0,
            0,
            0});
            this.maxAngleNumericUpDown.Name = "maxAngleNumericUpDown";
            this.maxAngleNumericUpDown.Size = new System.Drawing.Size(90, 20);
            this.maxAngleNumericUpDown.TabIndex = 11;
            this.maxAngleNumericUpDown.Value = new decimal(new int[] {
            180,
            0,
            0,
            0});
            this.maxAngleNumericUpDown.ValueChanged += new System.EventHandler(this.maxAngleNumericUpDown_ValueChanged);
            // 
            // minAngleNumericUpDown
            // 
            this.minAngleNumericUpDown.Location = new System.Drawing.Point(304, 39);
            this.minAngleNumericUpDown.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.minAngleNumericUpDown.Maximum = new decimal(new int[] {
            180,
            0,
            0,
            0});
            this.minAngleNumericUpDown.Name = "minAngleNumericUpDown";
            this.minAngleNumericUpDown.Size = new System.Drawing.Size(90, 20);
            this.minAngleNumericUpDown.TabIndex = 10;
            this.minAngleNumericUpDown.ValueChanged += new System.EventHandler(this.minAngleNumericUpDown_ValueChanged);
            // 
            // allAngleRadioButton
            // 
            this.allAngleRadioButton.AutoSize = true;
            this.allAngleRadioButton.Location = new System.Drawing.Point(220, 62);
            this.allAngleRadioButton.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.allAngleRadioButton.Name = "allAngleRadioButton";
            this.allAngleRadioButton.Size = new System.Drawing.Size(66, 17);
            this.allAngleRadioButton.TabIndex = 9;
            this.allAngleRadioButton.TabStop = true;
            this.allAngleRadioButton.Text = "Show All";
            this.allAngleRadioButton.UseVisualStyleBackColor = true;
            this.allAngleRadioButton.CheckedChanged += new System.EventHandler(this.allAngleRadioButton_CheckedChanged);
            // 
            // angleNotBetweenRadioButton
            // 
            this.angleNotBetweenRadioButton.AutoSize = true;
            this.angleNotBetweenRadioButton.Location = new System.Drawing.Point(220, 40);
            this.angleNotBetweenRadioButton.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.angleNotBetweenRadioButton.Name = "angleNotBetweenRadioButton";
            this.angleNotBetweenRadioButton.Size = new System.Drawing.Size(84, 17);
            this.angleNotBetweenRadioButton.TabIndex = 8;
            this.angleNotBetweenRadioButton.TabStop = true;
            this.angleNotBetweenRadioButton.Text = "not between";
            this.angleNotBetweenRadioButton.UseVisualStyleBackColor = true;
            this.angleNotBetweenRadioButton.CheckedChanged += new System.EventHandler(this.angleNotBetweenRadioButton_CheckedChanged);
            // 
            // angleBetweenRadioButton
            // 
            this.angleBetweenRadioButton.AutoSize = true;
            this.angleBetweenRadioButton.Location = new System.Drawing.Point(220, 17);
            this.angleBetweenRadioButton.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.angleBetweenRadioButton.Name = "angleBetweenRadioButton";
            this.angleBetweenRadioButton.Size = new System.Drawing.Size(66, 17);
            this.angleBetweenRadioButton.TabIndex = 7;
            this.angleBetweenRadioButton.TabStop = true;
            this.angleBetweenRadioButton.Text = "between";
            this.angleBetweenRadioButton.UseVisualStyleBackColor = true;
            this.angleBetweenRadioButton.CheckedChanged += new System.EventHandler(this.angleBetweenRadioButton_CheckedChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.swapDataGroupBox);
            this.Controls.Add(this.visibilityGroupBox);
            this.Controls.Add(this.sensorsGroupBox);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.sensor1GroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).EndInit();
            this.sensor3GroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown6)).EndInit();
            this.sensor2GroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown9)).EndInit();
            this.sensor4GroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown12)).EndInit();
            this.sensorsGroupBox.ResumeLayout(false);
            this.sensorsGroupBox.PerformLayout();
            this.visibilityGroupBox.ResumeLayout(false);
            this.visibilityGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown13)).EndInit();
            this.swapDataGroupBox.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.maxAngleNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.minAngleNumericUpDown)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.GroupBox sensor1GroupBox;
        private System.Windows.Forms.NumericUpDown numericUpDown3;
        private System.Windows.Forms.NumericUpDown numericUpDown2;
        private System.Windows.Forms.GroupBox sensor3GroupBox;
        private System.Windows.Forms.NumericUpDown numericUpDown4;
        private System.Windows.Forms.NumericUpDown numericUpDown5;
        private System.Windows.Forms.NumericUpDown numericUpDown6;
        private System.Windows.Forms.GroupBox sensor2GroupBox;
        private System.Windows.Forms.NumericUpDown numericUpDown7;
        private System.Windows.Forms.NumericUpDown numericUpDown8;
        private System.Windows.Forms.NumericUpDown numericUpDown9;
        private System.Windows.Forms.GroupBox sensor4GroupBox;
        private System.Windows.Forms.NumericUpDown numericUpDown10;
        private System.Windows.Forms.NumericUpDown numericUpDown11;
        private System.Windows.Forms.NumericUpDown numericUpDown12;
        private System.Windows.Forms.GroupBox sensorsGroupBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox visibilityGroupBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown numericUpDown13;
        private System.Windows.Forms.RadioButton allDistRadioButton;
        private System.Windows.Forms.RadioButton greaterDistRadioButton;
        private System.Windows.Forms.RadioButton lessDistRadioButton;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button swapBottom;
        private System.Windows.Forms.Button swapTop;
        private System.Windows.Forms.GroupBox swapDataGroupBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.NumericUpDown minAngleNumericUpDown;
        private System.Windows.Forms.RadioButton allAngleRadioButton;
        private System.Windows.Forms.RadioButton angleNotBetweenRadioButton;
        private System.Windows.Forms.RadioButton angleBetweenRadioButton;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown maxAngleNumericUpDown;
    }
}

