﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CosmicScannerUI
{
    public partial class Form1 : Form
    {
        MappedFile MMF { get; }

        public Form1()
        {
            InitializeComponent();
            MMF = new MappedFile(sizeof(float) * (int)VariablesPositions.Total, "CosmicScanMMF");
            allDistRadioButton.Checked = true;
            allAngleRadioButton.Checked = true;
            WriteDefaultValues();
        }

        private void WriteDefaultValues()
        {
            numericUpDown1_ValueChanged(numericUpDown1, null);
            numericUpDown2_ValueChanged(numericUpDown2, null);
            numericUpDown3_ValueChanged(numericUpDown3, null);
            numericUpDown4_ValueChanged(numericUpDown4, null);
            numericUpDown5_ValueChanged(numericUpDown5, null);
            numericUpDown6_ValueChanged(numericUpDown6, null);
            numericUpDown7_ValueChanged(numericUpDown7, null);
            numericUpDown8_ValueChanged(numericUpDown8, null);
            numericUpDown9_ValueChanged(numericUpDown9, null);
            numericUpDown10_ValueChanged(numericUpDown10, null);
            numericUpDown11_ValueChanged(numericUpDown11, null);
            numericUpDown12_ValueChanged(numericUpDown12, null);
            numericUpDown13_ValueChanged(numericUpDown13, null);
            maxAngleNumericUpDown_ValueChanged(maxAngleNumericUpDown, null);
            minAngleNumericUpDown_ValueChanged(minAngleNumericUpDown, null);

            allDistRadioButton_CheckedChanged(null, null);
            allAngleRadioButton_CheckedChanged(null, null);
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            MMF.WriteValue(VariablesPositions.Sensor0X, (float)numericUpDown1.Value);
        }

        private void numericUpDown2_ValueChanged(object sender, EventArgs e)
        {
            MMF.WriteValue(VariablesPositions.Sensor0Y, (float)numericUpDown2.Value);
        }

        private void numericUpDown3_ValueChanged(object sender, EventArgs e)
        {
            MMF.WriteValue(VariablesPositions.Sensor0Z, (float)numericUpDown3.Value);
        }

        private void numericUpDown9_ValueChanged(object sender, EventArgs e)
        {
            MMF.WriteValue(VariablesPositions.Sensor1X, (float)numericUpDown9.Value);
        }

        private void numericUpDown8_ValueChanged(object sender, EventArgs e)
        {
            MMF.WriteValue(VariablesPositions.Sensor1Y, (float)numericUpDown8.Value);
        }

        private void numericUpDown7_ValueChanged(object sender, EventArgs e)
        {
            MMF.WriteValue(VariablesPositions.Sensor1Z, (float)numericUpDown7.Value);
        }

        private void numericUpDown6_ValueChanged(object sender, EventArgs e)
        {
            MMF.WriteValue(VariablesPositions.Sensor2X, (float)numericUpDown6.Value);
        }

        private void numericUpDown5_ValueChanged(object sender, EventArgs e)
        {
            MMF.WriteValue(VariablesPositions.Sensor2Y, (float)numericUpDown5.Value);
        }

        private void numericUpDown4_ValueChanged(object sender, EventArgs e)
        {
            MMF.WriteValue(VariablesPositions.Sensor2Z, (float)numericUpDown4.Value);
        }

        private void numericUpDown12_ValueChanged(object sender, EventArgs e)
        {
            MMF.WriteValue(VariablesPositions.Sensor3X, (float)numericUpDown12.Value);
        }

        private void numericUpDown11_ValueChanged(object sender, EventArgs e)
        {
            MMF.WriteValue(VariablesPositions.Sensor3Y, (float)numericUpDown11.Value);
        }

        private void numericUpDown10_ValueChanged(object sender, EventArgs e)
        {
            MMF.WriteValue(VariablesPositions.Sensor3Z, (float)numericUpDown10.Value);
        }

        private void allDistRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            MMF.WriteValue(VariablesPositions.DistancePredicateType, (float)DistancePredicateTypes.ShowAll);
        }

        private void greaterDistRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            MMF.WriteValue(VariablesPositions.DistancePredicateType, (float)DistancePredicateTypes.GreaterThan);
        }

        private void lessDistRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            MMF.WriteValue(VariablesPositions.DistancePredicateType, (float)DistancePredicateTypes.LessThan);
        }

        private void numericUpDown13_ValueChanged(object sender, EventArgs e)
        {
            MMF.WriteValue(VariablesPositions.DistancePredicateLimit, (float)numericUpDown13.Value);
        }

        private void swapBottom_Click(object sender, EventArgs e)
        {
            MMF.WriteValue(VariablesPositions.SwapBottom, 1.0f);
        }

        private void swapTop_Click(object sender, EventArgs e)
        {
            MMF.WriteValue(VariablesPositions.SwapTop, 1.0f);
        }

        private void minAngleNumericUpDown_ValueChanged(object sender, EventArgs e)
        {
            MMF.WriteValue(VariablesPositions.AngleMin, DegreesToRadians((float)minAngleNumericUpDown.Value));
        }

        private void maxAngleNumericUpDown_ValueChanged(object sender, EventArgs e)
        {
            MMF.WriteValue(VariablesPositions.AngleMax, DegreesToRadians((float)maxAngleNumericUpDown.Value));
        }

        private void angleBetweenRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            MMF.WriteValue(VariablesPositions.AnglePredicateType, (float)AnglePredicateTypes.Between);
        }

        private void angleNotBetweenRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            MMF.WriteValue(VariablesPositions.AnglePredicateType, (float)AnglePredicateTypes.NotBetween);
        }

        private void allAngleRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            MMF.WriteValue(VariablesPositions.AnglePredicateType, (float)AnglePredicateTypes.ShowAll);
        }

        float DegreesToRadians(float degrees)
        {
            return degrees * (float)Math.PI / 180.0f;
        }
    }
}
