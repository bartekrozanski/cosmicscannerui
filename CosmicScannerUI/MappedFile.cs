﻿using System;
using System.Collections.Generic;
using System.IO.MemoryMappedFiles;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CosmicScannerUI
{
    enum DistancePredicateTypes
    {
        ShowAll,
        GreaterThan,
        LessThan,
    }

    enum AnglePredicateTypes
    {
        ShowAll,
        Between,
        NotBetween,
    }

    enum VariablesPositions
    {
        DataChanged,
        Sensor0X,
        Sensor0Y,
        Sensor0Z,
        Sensor1X,
        Sensor1Y,
        Sensor1Z,
        Sensor2X,
        Sensor2Y,
        Sensor2Z,
        Sensor3X,
        Sensor3Y,
        Sensor3Z,
        DistancePredicateType,
        DistancePredicateLimit,
        SwapTop,
        SwapBottom,
        AnglePredicateType,
        AngleMin,
        AngleMax,
        Total
    }

    class MappedFile
    {
        MemoryMappedFile MMF { get;}
        MemoryMappedViewAccessor Accessor { get; }

        public MappedFile(long size, string name)
        {
            MMF = MemoryMappedFile.CreateNew(
                name,
                size,
                MemoryMappedFileAccess.ReadWrite,
                MemoryMappedFileOptions.None,
                System.IO.HandleInheritability.None
                );
            Accessor = MMF.CreateViewAccessor(0, size);
        }

        public void WriteValue(VariablesPositions dest, float val)
        {
            Accessor.Write(sizeof(float) * (int)dest, val);
            Accessor.Write(sizeof(float) * (int)VariablesPositions.DataChanged, 1);
        }
    }
}
