For more information go to [CosmicScanner repo](https://gitlab.com/bartekrozanski/cosmicscanner/)  

# What is it?
This is a UI for [CosmicScanner](https://gitlab.com/bartekrozanski/cosmicscanner/) project.

# How to build?
Clone this repo and launch .sln file with visual studio.


